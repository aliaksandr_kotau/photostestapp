package com.mynetdiary.myapplication

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import coil.load
import com.mynetdiary.myapplication.model.Photo

private const val PHOTO_ARG = "photo_arg"

class PhotoDetailsFragment : Fragment(R.layout.fragment_photo_details) {

    companion object {
        fun newInstance(photo: Photo) = PhotoDetailsFragment().apply {
            arguments = bundleOf(PHOTO_ARG to photo)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val photo = arguments?.getParcelable<Photo>(PHOTO_ARG)

        view.findViewById<ImageView>(R.id.photo).load(photo?.url)
        view.findViewById<TextView>(R.id.title).text = photo?.title
    }
}