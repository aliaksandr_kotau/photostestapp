package com.mynetdiary.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.mynetdiary.myapplication.model.Photo

class PhotosAdapter : ListAdapter<Photo, PhotosAdapter.PhotoViewHolder>(PhotoItemDiffCallback()) {

    var listener: OnPhotoClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_photo, parent, false)
        return PhotoViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun swapPhotos(photos: List<Photo>) {
        submitList(photos)
    }

    inner class PhotoViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(photo: Photo) = with(view) {
            findViewById<ImageView>(R.id.photo).load(photo.thumbnailUrl)
            findViewById<TextView>(R.id.title).text = photo.title

            findViewById<ViewGroup>(R.id.root).setOnClickListener {
                listener?.onPhotoClick(photo)
            }
        }
    }
}

class PhotoItemDiffCallback : DiffUtil.ItemCallback<Photo>() {

    override fun areItemsTheSame(oldItem: Photo, newItem: Photo) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Photo, newItem: Photo) =
        oldItem.albumId == newItem.albumId
                && oldItem.thumbnailUrl == newItem.thumbnailUrl
                && oldItem.title == newItem.title
}

interface OnPhotoClickListener {
    fun onPhotoClick(photo: Photo)
}