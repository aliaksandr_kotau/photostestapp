package com.mynetdiary.myapplication

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE
import com.mynetdiary.myapplication.model.Photo
import com.mynetdiary.myapplication.navigation.NavigationListener

const val MAIN_BACK_STACK = "main_back_stack"

class MainActivity : AppCompatActivity(), NavigationListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val photosFragment = PhotosFragment.newInstance()
        photosFragment.navigationListener = this

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, photosFragment)
            .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportFragmentManager.popBackStackImmediate(MAIN_BACK_STACK, POP_BACK_STACK_INCLUSIVE)
    }

    override fun openDetails(photo: Photo) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, PhotoDetailsFragment.newInstance(photo))
            .addToBackStack(MAIN_BACK_STACK)
            .commit()
    }
}