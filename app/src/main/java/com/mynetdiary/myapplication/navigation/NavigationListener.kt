package com.mynetdiary.myapplication.navigation

import com.mynetdiary.myapplication.model.Photo

interface NavigationListener {
    fun openDetails(photo:  Photo)
}