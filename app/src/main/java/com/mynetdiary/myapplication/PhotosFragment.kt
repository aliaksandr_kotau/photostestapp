package com.mynetdiary.myapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mynetdiary.myapplication.model.Photo
import com.mynetdiary.myapplication.navigation.NavigationListener
import com.mynetdiary.myapplication.network.PhotosService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class PhotosFragment : Fragment(R.layout.fragment_photos), OnPhotoClickListener {

    private val photosService = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .client(OkHttpClient.Builder().build())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(PhotosService::class.java)

    private lateinit var disposable: Disposable

    var navigationListener: NavigationListener? = null

    companion object {
        fun newInstance() = PhotosFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PhotosAdapter()
        adapter.listener = this
        val layoutManager = LinearLayoutManager(context)

        val photosView = view.findViewById<RecyclerView>(R.id.photos)
        photosView.layoutManager = layoutManager
        photosView.adapter = adapter

        disposable = photosService.getPhotos()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ photos ->
                adapter.swapPhotos(photos)
            }, {})
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

    override fun onPhotoClick(photo: Photo) {
        navigationListener?.openDetails(photo)
    }
}