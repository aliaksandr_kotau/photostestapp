package com.mynetdiary.myapplication.network

import com.mynetdiary.myapplication.model.Photo
import io.reactivex.Single
import retrofit2.http.GET

interface PhotosService {
    @GET("photos")
    fun getPhotos(): Single<List<Photo>>
}



